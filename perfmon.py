# -*- coding: utf-8 -*-

import time
import re
import requests
import subprocess
import os
from datetime import datetime, timedelta
from random import randint
from gatilegrid import GeoadminTileGridLV95 as GeoadminTileGrid
from shutil import copyfile

session = requests.Session()
withProxies = False
proxies = {
  'http': 'http://proxy-bvcol.admin.ch:8080',
  'https': 'https://proxy-bvcol.admin.ch:8080'
}

insideSwiss = [2573750, 1150000, 2741250, 1253500]
gaGrid = GeoadminTileGrid()

class Stat:
    def __init__(self, prefix):
        self.init()
        self._prefix = prefix

    def add(self, code, elapsed):
        if code == 200:
            total = self.avgElapsed * self.succCount
            self.succCount = self.succCount + 1
            self.avgElapsed = (total + elapsed) / self.succCount
        else:
            self.failCount = self.failCount + 1

    def init(self):
        self.failCount = 0
        self.succCount = 0
        self.avgElapsed = 0
    
    def headerOut(self):
        return self._prefix + '_ms,' + self._prefix + '_s,' + self._prefix + '_f'
            
    def out(self):
        return str(int(self.avgElapsed * 1000)) + ',' + str(self.succCount) + ',' + str(self.failCount)

class TypeStat:
    def __init__(self):
        self._statCFHit = Stat('CFH')
        self._statCFMissVHit = Stat('CFMVH')
        self._statCFMissS3Hit = Stat('CFMVMS3H')
        self._statCFMissS3Miss = Stat('CFMVMS3M')
        self._statVHit = Stat('VH')
        self._statVS3Hit = Stat('VMS3H')
        self._statVS3Miss = Stat('VMS3M')

    def resetStats(self):
        self._statCFHit.init()
        self._statCFMissVHit.init()
        self._statCFMissS3Hit.init()
        self._statCFMissS3Miss.init()
        self._statVHit.init()
        self._statVS3Hit.init()
        self._statVS3Miss.init()

    def headerOut(self):
        return 'date,' + self._statCFHit.headerOut() + ',' + self._statCFMissVHit.headerOut() + ',' + self._statCFMissS3Hit.headerOut() + ',' + self._statCFMissS3Miss.headerOut() + ',' + self._statVHit.headerOut() + ',' + self._statVS3Hit.headerOut() + ',' + self._statVS3Miss.headerOut() + '\n'

    def out(self, date):
        return date + ',' + self._statCFHit.out() + ',' + self._statCFMissVHit.out() + ',' + self._statCFMissS3Hit.out() + ',' + self._statCFMissS3Miss.out() + ',' + self._statVHit.out() + ',' + self._statVS3Hit.out() + ',' + self._statVS3Miss.out() + '\n'


defs = [{
    'type': 'wmts',
    'protocol': 'http',
    'baseUrls': ['wmts.geo.admin.ch', 'wmts100.geo.admin.ch'],
    'layer': 'ch.swisstopo.swissimage',
    'zooms': [16, 23, 28],
    'ext': 'jpeg',
    'stat': TypeStat()
    },{
    'type': 'wmts',
    'protocol': 'https',
    'baseUrls': ['wmts.geo.admin.ch', 'wmts100.geo.admin.ch'],
    'layer': 'ch.swisstopo.pixelkarte-farbe',
    'zooms': [16, 23, 27],
    'ext': 'jpeg',
    'stat': TypeStat()
    },{
    'type': 'wmts',
    'protocol': 'https',
    'baseUrls': ['wmts.geo.admin.ch', 'wmts100.geo.admin.ch'],
    'layer': 'ch.swisstopo.swisstlm3d-wanderwege',
    'zooms': [16, 23, 26],
    'ext': 'png',
    'stat': TypeStat()
    },{
    'type': 'wmts',
    'protocol': 'https',
    'baseUrls': ['wmts.geo.admin.ch', 'wmts100.geo.admin.ch'],
    'layer': 'ch.bfe.solarenergie-eignung-daecher',
    'zooms': [16, 23, 26],
    'ext': 'png',
    'stat': TypeStat()
}]

def upload():
    for defi in defs:
        l = defi['layer']
        try:
            sub = subprocess.check_output('aws s3 sync --cache-control "public, max-age=120" --profile ltjeg_aws_admin ' + l + ' s3://cms.geo.admin.ch/stats/perfmon/' + l, shell = True)
            print(sub)

        except Exception as e:
            print('exception while trying to upload...', e)


def write_out():
    now = datetime.now()
    # every hour
    if now.minute == 0:
        for defi in defs:
            l = defi['layer']
            protocol = defi['protocol']
            stat = defi['stat']
            if not os.path.exists(l):
                os.makedirs(l)
            dfile = l + '/' + protocol + '_day.csv'
            yfile = l + '/' + protocol + '_' + str(now.year) + '.csv'
         
            # check if just after midnight
            if now.hour == 0:
                # reset daily file
                if os.path.isfile(dfile):
                    copyfile(dfile, l + '/' + protocol + '_' + now.strftime('%Y%m%d') + '.csv')
                os.remove(dfile)

                # write year file (once a day)
                if not os.path.isfile(yfile):
                    with open(yfile, 'a+') as yf:
                        yf.write(stat.headerOut())
                with open(yfile, 'a+') as yf:
                    yf.write(stat.out(now.strftime('%d.%m.%Y')))

            # write to daily file
            if not os.path.isfile(dfile):
                with open(dfile, 'a+') as df:
                    df.write(stat.headerOut())
                # write to year file

            with open(dfile, 'a+') as df:
                df.write(stat.out(now.strftime('%d.%m.%Y %H:%M')))

            upload()
            #reset counters each hour
            stat.resetStats()

        time.sleep(61.0)  

def randomEntry(ar):
    return ar[randint(0, len(ar) -1)]

def randomCoordinates():
    return [randint(insideSwiss[0],insideSwiss[2]), randint(insideSwiss[1], insideSwiss[3])]

def randomTile(z):
    tile = gaGrid.tileAddress(z, randomCoordinates())
    return '' + str(z) + '/' + str(tile[0]) + '/' + str(tile[1])

def getRandomTile(entry):
    return entry['protocol'] + '://' + randomEntry(entry['baseUrls']) + '/1.0.0/' + entry['layer'] + '/default/current/2056/' + randomTile(randomEntry(entry['zooms'])) + '.' + entry['ext']

def parse_stat(stat, code, xcache, s3cache, time):
    print(xcache, s3cache)
    if 'cloudfront' in xcache:
        if 'Hit' in xcache:
            stat._statCFHit.add(code, time)
        elif 'HIT' in xcache:
            stat._statCFMissVHit.add(code, time)
        elif 'Miss' in xcache:
            if 'HIT' in s3cache:
                stat._statCFMissS3Hit.add(code, time)
            else:
                stat._statCFMissS3Miss.add(code,time)
        else:
            raise Exception('No Miss or Hit in x-cache')
    elif 'MISS' in xcache:
        if 'HIT' in s3cache:
            stat._statVS3Hit.add(code, time)
        elif 'MISS' in s3cache:
            stat._statVS3Miss.add(code, time)
        else:
            raise Exception('No MISS or HIT or cloudfront in s3-cache header')
    elif 'HIT' in xcache:
        stat._statVHit(code, time)
    else:
        raise Exception('No MISS or HIT or cloudfront in x-cache')

def do_request(tile, stat):
    try:
        r = session.get(tile, headers = {'referer': 'https://map.geo.admin.ch/?ltjeg=gjn', 'User-Agent': 'PerformanceMonitorBot/jeg/gjn'}, proxies = proxies if withProxies else None);
        elapsed = r.elapsed.total_seconds()
        parse_stat(stat, r.status_code, r.headers['X-Cache'], r.headers['X-tiles-s3-cache'], elapsed)

    except Exception as e:
        print('exception thrown with python, but continue...', e)
 


def main():
    while True:
        entry = randomEntry(defs)
        tile = getRandomTile(entry)
        stat = entry['stat']
        do_request(tile, stat)
        print(tile)
        print(stat.out(datetime.now().strftime('%d.%m.%Y %H:%M')))
        write_out()
        time.sleep(1.0)
        
if __name__ == '__main__':
    main()
